#!/usr/bin/env bash

WALLPAPER="/home/lukser/Pictures/Wallpapers/background.png"


configure_input () {
	echo "Configuring Input"
}


# Panel
[[ $(pgrep -f waybar) ]] && killall waybar
waybar &

# Notification Manager
[[ ! $(pgrep -f mako) ]] && mako &

# Volume & Brightness Notifications
[[ ! $(pgrep -f avizo-service) ]] && avizo-service &

# Polkit
[[ ! $(pgrep -f lxpolkit) ]] && lxpolkit &

# Wallpapers
swaybg -i $WALLPAPER &

# Displays
if [[ ! $(pgrep -f lxpolkit) ]]; then
    way-displays > /tmp/way-displays.${XDG_VTNR}.${USER}.log 2>&1 &
fi
