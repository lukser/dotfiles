#!/usr/bin/env -S v run
module main

import os
import cli

fn total() int {
	return os
		.execute_or_panic("free -m | grep 'Mem:' | awk '{ print $2 }'")
		.output
		.int()
}

fn used() int {
	return os
		.execute_or_panic("free -m | grep 'Mem:' | awk '{ print $3 }'")
		.output
		.int()
}

fn free_mem() int {
	return total() - used()
}

fn percent_used() f32 {
	return f32(used()) / f32(total()) * 100.0
}

fn main() {
	mut app := cli.Command{
		name: "memory"
		description: "Memory helper"
		execute: fn (cmd cli.Command) ? {
			println(int(percent_used()))
			return
		}
		commands: [
			cli.Command{
				name: "total"
				execute: fn (cmd cli.Command) ? {
					println(total())
					return
				}
			},
			cli.Command{
				name: "used"
				execute: fn (cmd cli.Command) ? {
					println(used())
					return
				}
			},
			cli.Command{
				name: "free"
				execute: fn (cmd cli.Command) ? {
					println(free_mem())
					return
				}
			},
		]
	}
	app.setup()
	app.parse(os.args)
}