
if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_greeting
    echo Welcome, (set_color red)Master(set_color normal)! I hope we could (set_color blue)fish(set_color normal) together...
end

alias ls="exa --icons"
alias l="ls"
alias ll="exa --icons -lh"
alias la="exa --icons -lha"

source ~/.asdf/asdf.fish
fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/go/bin
fish_add_path $HOME/.nimble/bin
fish_add_path $HOME/.local/share/neovim/bin
fish_add_path $HOME/.local/bin


starship init fish | source

### MANAGED BY RANCHER DESKTOP START (DO NOT EDIT)
set --export --prepend PATH "/home/lukser/.rd/bin"
### MANAGED BY RANCHER DESKTOP END (DO NOT EDIT)
